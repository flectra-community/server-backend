# Flectra Community / server-backend

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[base_user_role](base_user_role/) | 2.0.2.3.0| User roles
[base_import_match](base_import_match/) | 2.0.1.0.0| Try to avoid duplicates before importing
[base_global_discount](base_global_discount/) | 2.0.1.0.0| Base Global Discount
[base_user_role_company](base_user_role_company/) | 2.0.2.0.1| User roles by company
[base_user_role_profile](base_user_role_profile/) | 2.0.1.0.0| User profiles


