# Copyright <2011> <Daniel Reis, , Jamotion GmbHMaxime Chambreuil, Savoir-faire Linux>
# Copyright <2016> <Henry Zhou MAXflectra>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "External Database Sources",
    "version": "2.0.2.0.0",
    "category": "Tools",
    "author": "Daniel Reis, , Jamotion GmbH" "LasLabs, " "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "LGPL-3",
    "images": ["images/screenshot01.png"],
    "depends": ["base"],
    "data": [
        "views/base_external_dbsource.xml",
        "security/ir.model.access.csv",
        "security/base_external_dbsource_security.xml",
    ],
    "demo": ["demo/base_external_dbsource.xml"],
    "installable": True,
}
